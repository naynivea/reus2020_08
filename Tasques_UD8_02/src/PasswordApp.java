import modelo.Password;

/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class PasswordApp {

	/**
	 * 
	 */
	public static void main(String[] args) {
		Password pass1 = new Password();
		Password pass2 = new Password(10);
		
		System.out.println("Password 1: " + pass1.getContrasena());
		System.out.println("Password 2: " + pass2.getContrasena());
	}

}
