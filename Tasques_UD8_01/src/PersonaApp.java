import modelo.Persona;

/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class PersonaApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Persona persona1 = new Persona("55289587k");
		Persona persona2 = new Persona("Claudia", 20, "54285567l", 'M');
		Persona persona3 = new Persona("Maria", 40, "53289588k",'M', 63.8, 1.65);

		System.out.println(persona1.toString());
		System.out.println(persona2.toString());
		System.out.println(persona3.toString());

		
	}

}
